
var form = $("#registrationForm").show();
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    labels:
        {
            prev: "Previous",
            next: "Forward",
            finish: "Submit",
        },
    onStepChanging: function (event, currentIndex, newIndex) {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18) {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
        $("#currentStep").text(currentIndex + 1 + "/ 3");
        setTimeout(function () { resizeJquerySteps(); }, 50);
    },
    onFinishing: function (event, currentIndex) {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex) {
        $(".form-box").fadeOut();
        $(".success-message").fadeIn();
        $(".checkmark").addClass("animate");
    }
}).validate({
    rules: {
        firstName: {
            required: true,
        },
        lastName: {
            required: true,
        },
        email: {
            required: true,
            email: true
        },
        age: {
            required: true,
            number: true
        },
        telephone: {
            required: true,
            minlength: 9,
            maxlength: 10,
            number: true
        },
        gender: {
            required: true,
        },
        address: {
            required: true
        },
        city: {
            required: true
        },
        pincode: {
            required: true,
            number: true
        },
        country: {
            required: true,
        },
        textarea: {
            required: true,
        },
        terms: {
            required: true,
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "gender"){
            error.insertBefore(".radio-button-list > li:first-child");
        }
        else if (element.attr("name") == "terms") {
            var closestcheckbox = element.closest(".ci-custom-checkbox");
            error.insertBefore(closestcheckbox);
        }
        else{
            error.insertBefore(element);
        }
    },
    highlight: function (element) {
        $(element).closest(".radio-button-list").addClass("error");
    },
    unhighlight: function (element) {
        $(element).closest(".radio-button-list").removeClass("error");
    },
    // To dynamically change the height of '.content' when error message appears
    invalidHandler: function (form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            setTimeout(function () { resizeJquerySteps(); }, 50);
        }
    }
});

function resizeJquerySteps() {
    $('.wizard .content').animate({ height: $('.body.current').outerHeight() }, "slow");
}
resizeJquerySteps();