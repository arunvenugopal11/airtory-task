module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: { // Task
      dist: { // Target
        options: { // Target options
          style: 'expanded'
        },
        files: { // Dictionary of files
          'css/main.css': 'scss/main.scss',

        }
      }
    },

    watch: {
      sass: {
        files: "scss/**/*.scss",
        tasks: ['sass'],
        options: {
          spawn: false,
        },
      },
    },
    browserSync: {
      bsFiles: {
        src: [
          'scss/**/*.scss',
          '*.html'
        ]
      },
      options: {
        server: {
          baseDir: "./"
        },
        watchTask: true,
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  // define default task
  grunt.registerTask('default', ['browserSync', 'watch']);

};
